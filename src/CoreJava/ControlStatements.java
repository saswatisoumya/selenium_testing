package CoreJava;

public class ControlStatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * int a =5, b=3;
		 * 
		 * if(a>b) { System.out.println("a is greater then b"); }
		 */

		// switch statement

		int a = 2;
		switch (a) {
		case 0:
			System.out.println("inside case 0");
			break;

		case 2:
			System.out.println("inside case 2");
			break;

		default:
			System.out.println("inside default");

		}

	}

}
