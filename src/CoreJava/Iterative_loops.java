package CoreJava;

public class Iterative_loops {

	public static void main (String args[])
	{
		
		int i=0;
		
		while (i<5) {
			
			if(i==3) {
				i++;
				continue;
			}
			System.out.println(" Inside while loop "+i);
			i++;
		}
	}
}
