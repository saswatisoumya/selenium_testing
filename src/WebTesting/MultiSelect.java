package WebTesting;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.server.SeleniumCommandTimedOutException;
import org.openqa.selenium.support.ui.Select;

public class MultiSelect {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
	   System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
       WebDriver driver = new ChromeDriver();
       driver.manage().window().maximize();
       
       driver.get("https://www.krninformatix.com/sample.html");
       
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
       
       WebElement Multiselect =driver.findElement(By.id("cities"));
       
       Select dd = new Select(Multiselect);
       dd.selectByIndex(0);
       Thread.sleep(3000);
       dd.selectByValue("2");
       Thread.sleep(4000);
       dd.selectByVisibleText("Mumbai");
       
       //Thread.sleep(2000);
       //dd.deselectAll();
       //System.out.println("completed");
       
       List<WebElement> alloptions =dd.getOptions();// to get all options
       //List<WebElement> alloptions =dd.getAllSelectedOptions(); // prints only the selected ones
       int si= alloptions.size();
      
       for (int i=0; i<si; i++)
       {
    	   
    	   WebElement option = alloptions.get(i);
    	   String te = option.getText();
    	   System.out.println(te);
    	   
       }
       
       
	}

}
