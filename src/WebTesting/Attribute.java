package WebTesting;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Attribute {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.krninformatix.com/sample.html");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        String attribute = driver.findElement(By.xpath("//a[contains(text(),'Google')]")).getAttribute("href");
        System.out.println(attribute);
        
	}

}
