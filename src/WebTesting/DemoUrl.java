package WebTesting;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class DemoUrl {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
       
		File pathToBinary = new File("C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		
		// Create WebDriver insance using firefox and profile.
		WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

		// Make a call to this URL using Firefox
		driver.get("https://demo.actitime.com/login.do");
		
		driver.findElement(By.id("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.id("loginButton")).click();
		
		Thread.sleep(3000);
		
		String ExpectedUrl = "https://demo.actitime.com/user/submit_tt.do";
		String ActualUrl = driver.getCurrentUrl();
		
		System.out.println(ActualUrl);
		
		if(ActualUrl.equals(ExpectedUrl))
		{
		
          System.out.println("pass");		

	    }
		else {
			System.out.println("fail");
			
		}
		
		
	}

}
