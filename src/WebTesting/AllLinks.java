package WebTesting;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AllLinks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
	       WebDriver driver = new ChromeDriver();
	       driver.manage().window().maximize();
	       
	       driver.get("https://www.krninformatix.com");
	       
	       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	       
	       
	          List<WebElement>  allinks =driver.findElements(By.xpath("//a"));
	           int si = allinks.size();
	           
	           // Prints all the links href present in the website //a is the anchor tag
	          for(int i=0; i<si;i++)
	          { 
	        	  WebElement link = allinks.get(i);
	        	  String linktext = link.getText();
	        	  System.out.println(linktext);
	        	  
	        	  
	          }
	}

}
