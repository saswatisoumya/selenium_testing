package WebTesting;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class DemoCheckbox {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		File pathToBinary = new File("C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		
		// Create WebDriver insance using firefox and profile.
		WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

		
        //System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\geckodriver-v0.30.0-win32\\geckodriver.exe");
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		
        
        
       
		// Make a call to this URL using Firefox
		driver.get(" https://www.krninformatix.com/sample.html");
		
		boolean checkBoxStatus = driver.findElement(By.id("rem")).isSelected();
	    System.out.println(checkBoxStatus );
	    
	    Thread.sleep(4000);
	    driver.findElement(By.id("rem")).click();
	    boolean checkBoxStatus1 = driver.findElement(By.id("rem")).isSelected();
	    System.out.println(checkBoxStatus1);
	    
	    Thread.sleep(4000);
	    driver.findElement(By.id("rem")).click();
	    boolean checkBoxStatus2 = driver.findElement(By.id("rem")).isSelected();
	    System.out.println(checkBoxStatus2);
	    
	    Thread.sleep(2000);
	    if(!checkBoxStatus2)
	    {
	    	driver.findElement(By.id("rem")).click();
	    }
	    
	    
	}

	
}
