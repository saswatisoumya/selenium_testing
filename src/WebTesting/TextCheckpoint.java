package WebTesting;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class TextCheckpoint {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		File pathToBinary = new File("C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		
		// Create WebDriver insance using firefox and profile.
		WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

		// Make a call to this URL using Firefox
		driver.get("https://demo.actitime.com/login.do");
		
		driver.findElement(By.id("username")).sendKeys("admin");
		driver.findElement(By.name("pwd")).sendKeys("manager");
		driver.findElement(By.id("loginButton")).click();
		
		Thread.sleep(3000);
		
		String ExpetedText = "Enter Time-Track";
		String ActualText = driver.findElement(By.xpath("//td[contains(text(),'Enter Time-Track')]")).getText();
		
		System.out.println(ActualText);
		
		if(ActualText.equals(ExpetedText))
		{
		
          System.out.println("pass");		

	    }
		else {
			System.out.println("fail");
			
		}

}

}