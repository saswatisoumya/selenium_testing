package WebTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		

		
System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");


WebDriver driver = new org.openqa.selenium.chrome.ChromeDriver();
driver.get("https://demoqa.com/tool-tips"); 
		System.out.println("demoqa webpage Displayed"); 
		
		//Maximise browser window 
		driver.manage().window().maximize(); 
		
		//Instantiate Action Class 
		Actions actions = new Actions(driver); 
		
		//Retrieve WebElement 
		WebElement element = driver.findElement(By.id("tooltipDemo")); 
		
		// Use action class to mouse hover 
		actions.moveToElement(element).perform(); 
		
		WebElement toolTip = driver.findElement(By.cssSelector(".tooltiptext")); 
		
		// To get the tool tip text and assert 
		String toolTipText = toolTip.getText();
		System.out.println("toolTipText-->"+toolTipText); 
	}

}
