package WebTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertBox {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		
		driver.get("https://krninformatix.com/selenium/testing.html");
		
		// click on alertbox click ok
		//click on alert box and get the text
		driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		
		driver.findElement(By.xpath("//button[text()='Generate Alert Box']")).click();
		String gettext = driver.switchTo().alert().getText();
		System.out.println(gettext);
		driver.switchTo().alert().accept();
		
	}

}
