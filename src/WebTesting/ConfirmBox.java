package WebTesting;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ConfirmBox {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		
		driver.get("https://krninformatix.com/selenium/testing.html");
		
		// click on alertbox click ok
		//click on alert box and get the text
		driver.findElement(By.xpath("//button[text()='Generate Confirm Box']")).click();
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		
	
	    Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Generate Confirm Box']")).click();
		String gettext = driver.switchTo().alert().getText();
		System.out.println(gettext);
		Thread.sleep(3000);
		driver.switchTo().alert().dismiss();
		System.out.println("operation completed");

	}

}
