package WebTesting;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.jetty.html.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class SingleSelect {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		File pathToBinary = new File("C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		
		// Create WebDriver insance using firefox and profile.
		WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

		driver.get("https://www.krninformatix.com/sample.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//finding the web element
		WebElement singleselect = driver.findElement(By.id("city"));
		// creating an instance of class select
		org.openqa.selenium.support.ui.Select dd = new org.openqa.selenium.support.ui.Select(singleselect);
		dd.selectByIndex(0);
		Thread.sleep(3000);
		dd.selectByValue("4");
		Thread.sleep(3000);
		dd.selectByVisibleText("Mumbai");
	   //System.out.println("action complted");
		
		//the action of selecting mumbai ,banngalore ,delhi is completed now and we need to print them.
		//as we have to print a list of all elements we need to keep it in list 
		
		List<WebElement> alloptions = dd.getOptions(); //getoptions is to select the list
		
		int si = alloptions.size(); 
		
		for (int i=0;i<si;i++)
		{
		      WebElement option = alloptions.get(i);
		      //System.out.println(option);
		      String te= option.getText();
		      System.out.println(te);
		}
		
		
		
				
		
	}

}
