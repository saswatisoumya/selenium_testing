package WebTesting;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class DemoTitle {
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
	
	File pathToBinary = new File("C:\\Users\\sahus15\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
	FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
	FirefoxProfile firefoxProfile = new FirefoxProfile();
	
	// Create WebDriver insance using firefox and profile.
	WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

	// Make a call to this URL using Firefox
	driver.get("https://demo.actitime.com/login.do");
	
	String expectedTitle = "actiTIME - Login";
	String actualTitle = driver.getTitle();
	
	if(actualTitle.equals(expectedTitle))
	{
	System.out.println("pass");
	}
	else
		
	{
	System.out.println("fail");
	}
	}

}
