package PracticalAssignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyntraSearch {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
	       WebDriver driver = new ChromeDriver();
	       driver.manage().window().maximize();
	       
	       driver.get("https://www.myntra.com/");
	       
	       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	       
	       //open myntra 1)select kurta sets 2)filter by radio women 3)click brand checkbox house of patudi
	       //4)select price range 824.0 TO 2118.0 5)select colour pink

	       
	       //1)select kurta sets
	       WebElement search =driver.findElement(By.xpath("//input[@class='desktop-searchBar']"));
	       search.sendKeys("kurta set\n");
	       
	       Thread.sleep(3000);
	       
	       
	       
	       //2)click on radio button women//error element not interactable
	       
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[2]/ul/li[2]/label")).click();
	       //WebElement radio=   driver.findElement(By.xpath("//input[@type='radio'][@value='women,men women']"));
	        //boolean womenradio = radio.isSelected();
	       //System.out.println(womenradio);
	      
	       //radio.click();
	       
	       //boolean checkRadioStatus1 =driver.findElement(By.xpath("//input[@type='radio'][@value='men,men women']")).isSelected();
	       //System.out.println(checkRadioStatus1);
	       
	       //driver.findElement(By.xpath("//input[@type='radio'][@value='men,men women']")).click();
	       
	     
	       //3)select a brand house of pataudi //element not interactable
	       WebElement brandsearch =  driver.findElement(By.xpath("//span[@class='myntraweb-sprite filter-search-iconSearch sprites-search']"));
	       brandsearch.click();
	       Thread.sleep(2000);
	       driver.findElement(By.xpath("//input[@class=\"filter-search-inputBox\"]")).sendKeys("House of Pataudi");
	       Thread.sleep(3000);
	       
	    
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[3]/ul/li[1]/label")).click();
	       
	     
	       
	       
	       
	        //4)select price range 1199
	       Thread.sleep(3000);
	      
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[4]/ul/li[1]/label")).click();
	       
	       
	       //choose colour pink
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[5]/div[1]/span")).click();
	       Thread.sleep(2000);
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[5]/div[1]/input")).sendKeys("pink");
	       driver.findElement(By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[5]/ul/li/label")).click();
	       
	       
	}

}


/*driver.findElement(By.xpath("//p[text()='FLAT ₹300 OFF']")).click();
Thread.sleep(3000);
driver.findElement(By.xpath("//div[text()='SIGN UP NOW >']")).click();
Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"reactPageContent\"]/div/div/div[2]/div[2]/div[1]/input")).sendKeys("9090529025");

Thread.sleep(1000);
driver.findElement(By.xpath("//*[@id=\"reactPageContent\"]/div/div/div[2]/div[2]/div[3]")).click();
*/