package PracticalAssignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Myntra {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://www.myntra.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// open myntra 1)select kurta sets 2)filter by radio women 3)click brand
		// checkbox house of patudi
		// 4)select price range 824.0 TO 2118.0 5)select colour pink

		// 1)select kurta sets
		WebElement search = driver.findElement(By.xpath("//input[@class='desktop-searchBar']"));
		search.sendKeys("kurta set\n");
		Thread.sleep(3000);
		
		// 2)click on radio button women//error element not intractable
		driver.findElement(By.xpath("//label[text()='Men']")).click();

		

		// 3)select a brand house of pataudi //element not interactable
		WebElement brandsearch = driver
				.findElement(By.xpath("//span[@class='myntraweb-sprite filter-search-iconSearch sprites-search']"));
		brandsearch.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@class=\"filter-search-inputBox\"]")).sendKeys("House of Pataudi");
		Thread.sleep(3000);

		boolean checkboxbrand = driver.findElement(By.xpath("//input[@type='checkbox'][@value='House of Pataudi']"))
				.isSelected();
		System.out.println(checkboxbrand);
		driver.findElement(By.xpath("//input[@type='checkbox'][@value='House of Pataudi']")).click();

		boolean checkboxbrandstatus = driver
				.findElement(By.xpath("//input[@type='checkbox'][@value='House of Pataudi']")).isSelected();
		System.out.println(checkboxbrandstatus);

		// 4)select price range 824.0 TO 2118.0
		Thread.sleep(3000);
		boolean checkboxprice = driver.findElement(By.xpath("//input[@type='checkbox'][@value='824.0 TO 2118.0']"))
				.isSelected();
		System.out.println(checkboxprice);
		driver.findElement(By.xpath("//input[@type='checkbox'][@value='824.0 TO 2118.0']")).click();

		// *[@id="mountRoot"]/div/div[1]/main/div[3]/div[1]/section/div/div[2]/ul/li[2]/label/input

	}

}
