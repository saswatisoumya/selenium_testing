package Popups;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AlertConfirmationPopup {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\sahus15\\Desktop\\JAVA\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		
		driver.get("https://www.krninformatix.com/selenium/testing.html");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		Actions action = new Actions(driver);
		
		action.moveToElement(driver.findElement(By.xpath("//button[text()='Double-click to generate alert box']"))).doubleClick().perform();
		driver.switchTo().alert().accept();
		
		Thread.sleep(3000);
		
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//button[text()='Generate Confirm Box']")).click();
		//driver.switchTo().alert().accept();
		Thread.sleep(2000);
		String Text = driver.switchTo().alert().getText();
		System.out.println(Text);
		driver.switchTo().alert().dismiss();
		
		driver.close();
		
		
	}

}
