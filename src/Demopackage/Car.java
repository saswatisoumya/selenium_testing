package Demopackage;

public class Car implements Vehicle {

	String fuelType;
	String modelName;
	protected int mileage;
	int seater;
	 
	public void drive() {
		System.out.println("Car is driving");
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		if(mileage<1) {
			System.out.println("Invalid Mileage set.");
		}else {
			this.mileage = mileage;
		}
	}

	public int getSeater() {
		return seater;
	}

	public void setSeater(int seater) {
		this.seater = seater;
	}
	
	

}
